#
# Michel Caillat - 12 juin 2019
#
# We will use Ubuntu for our image
FROM ubuntu:latest

# Updating Ubuntu packages
RUN apt-get update && yes|apt-get upgrade
RUN apt-get install -y emacs

# Adding cmake
RUN apt-get install -y cmake

# Adding git
RUN apt-get install -y git

# Adding wget and bzip2
RUN apt-get install -y wget bzip2

# Adding g++
RUN apt-get install -y g++

# Adding gfortran
RUN apt-get install -y gfortran

#Adding libboost-python-dev
RUN apt-get install -y libboost-python-dev

#Adding libboost-numpy-dev
RUN apt-get install -y libboost-numpy-dev

# Adding sudo
RUN apt-get -y install sudo

# Add user partemix with no password, add to sudo group
RUN adduser --disabled-password --gecos '' partemix
RUN adduser partemix sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
USER partemix
WORKDIR /home/partemix/
RUN chmod a+rwx /home/partemix/

# Adding Anaconda2 to partemix account
RUN wget https://repo.anaconda.com/archive/Anaconda2-2019.03-Linux-x86_64.sh
RUN bash Anaconda2-2019.03-Linux-x86_64.sh -b
RUN rm Anaconda2-2019.03-Linux-x86_64.sh

# Set path to conda
ENV PATH /home/partemix/anaconda2/bin:$PATH

RUN echo "PATH=$PATH"

# Updating Anaconda packages
RUN find /home/partemix/anaconda2 -name "conda"
RUN conda update conda
RUN conda update anaconda
RUN conda update --all

# The root directory of the FITS files location (readonly). It'll have to be bound to an host directory.
RUN mkdir /home/partemix/FITS

# Adding matplotlib
RUN pip install matplotlib==2.2.4

# Clone PyBDSF
RUN git clone https://github.com/lofar-astron/PyBDSF.git

# Install PyBDSF
WORKDIR PyBDSF
RUN pip install .
