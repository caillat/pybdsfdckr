# PyBDSFDckr

A "dockerized" version of the source finder application PyBDSF. 

cf. https://www.astron.nl/citt/pybdsf/

## Rationale
The rationale behind this project is to provide users interested in the astron/lofar application PyBDSF developped/maintained by David Rafferty 
with an ( hopefully ) easy way to execute the application independently of the characteristics of the execution platform. 

The price to pay is
to have `docker` installed and the time to build the image ( à priori only once ), but we think that this is reasonable in comparison to the service provided.

## Only two files !
Since the noble part is on the PyBDSF repository, the project is reduced to ... two files :
* the Dockerfile which will instruct docker to :
  * build an environment where PyBDSF runs gently
  * clone the PyBDSF project and install it
* the README.mf file which you are reading now

## Download and install
### Clone
```
cd <..>
git clone https://gitlab.obspm.fr/caillat/pybdsfdckr.git
```

### Build the image
```
cd pybdsfdckr
docker build -t mybdsf .
```

Of course, the choice of the tag ( `mybdsf` here ) is left to the user.

> Attention ! the build step may take some time (up to 14 minutes on a laptop Dell Latitude connected to the Internet with a domestic fiber link). You have time for a coffee break

### Create the container.
```
docker run -v /home/caillat/FITS/ALMA:/home/partemix/FITS --env="DISPLAY" -v /tmp/.X11-unix:/tmp/.X11-unix --net=host -it mybdsf
```

Remarks:
* In the example above we make the assumption that the FITS files are located somewhere in the directory `/datartemix/FITS` on the host; it will have to be adapted.
But the directory `/home/partemix/FITS` ( location on the container filesystem ) must be left unchanged.
* Pay attention to the sequence of options `--env="DISPLAY"-v /tmp/.X11-unix:/tmp/.X11-unix --net=host`; those are required if you plan to visualize the results of the execution ( command `show_fit` in the `pybdsf` command interpreter )


Then a shell is waiting for interactive input :

```
To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.

partemix@villoury:~/PyBDSF$
```

### Start `pybdsf` in the container

In the shell :

```
partemix@villoury:~/PyBDSF$ pybdsf

PyBDSF version 1.9.0
========================================================================
PyBDSF commands
  inp task ............ : Set current task and list parameters
  par = val ........... : Set a parameter (par = '' sets it to default)
                          Autocomplete (with TAB) works for par and val
  go .................. : Run the current task
  default ............. : Set current task parameters to default values
  tput ................ : Save parameter values
  tget ................ : Load parameter values
PyBDSF tasks
  process_image ....... : Process an image: find sources, etc.
  show_fit ............ : Show the results of a fit
  write_catalog ....... : Write out list of sources to a file
  export_image ........ : Write residual/model/rms/mean image to a file
PyBDSF help
  help command/task ... : Get help on a command or task
                          (e.g., help process_image)
  help 'par' .......... : Get help on a parameter (e.g., help 'rms_box')
  help changelog ...... : See list of recent changes
________________________________________________________________________

BDSF [1]: 
```


### Limitations

This is a remark about how PyBDSF manages the files that it produces during its execution. These files are created in the directory where the processed 
FITS file is located; this behaviour is not always possible when the FITS files are stored in a readonly space. In such situations PyBDSF must be given a copy of the FITS 
file of interest stored in a location where it can write the result files.

